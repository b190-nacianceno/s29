//  SECTION Comparision Query Operators
/* 
$gt/gte operator
    -allows us to find documents that have field number values greater than or greathan or equal to a specified number
SYNTAX: 
    db.collectionName.find({field: {$gt: value}});
    db.collectionName.find({field: {$gte: value}});
*/

db.users.find({age:{$gt: 50}});
db.users.find({age:{$gte: 50}});

// $lt/$lte operator
/* 
-allows us to find documents that have field number values less than or less than or equal to a specified number
SYNTAX: 
    db.collectionName.find({field: {$ls: value}});
    db.collectionName.find({field: {$lse: value}});
*/

db.users.find({age:{ $lt:50}});
db.users.find({age:{ $lte:50}});

//$ne -  Not equal to
db.users.find({age:{ $ne:50}});


// $in operator - find documents with specific math criteria on one field using diff values
db.users.find({lastName:{ $in:["Hawking","Doe"]}});
// when looking for a nested array
db.users.find({courses:{ $in:["HTML","React"]}});

// SECTION - Logical Query Operators
// $or operator
/* 
allows us to find documents matching a single criteria from multiple provided search criteria
SYNTAX:
    db.collectionName.find($or: [{{fieldA: valueA}, {fieldB: valueB}]});
*/

db.users.find({$or: [{firstName: "Neil"},{age: 21}]});

// try to fuse $or operator and gt operator (used in age)

db.users.find({$or:[{firstName: "Neil"}, {age: {$gt: 21}}]});

// $and operator
/* 
-allows us to find documents matching multiple criteria in a single field
SYNTAX:
    db.collectionName.find($and: [{{fieldA: valueA}, {fieldB: valueB}]});

*/

db.users.find({$and:[{age: {$ne:82}}, {age: {$ne:76}}]});

// SECTION - Field Projection
/* 
-retrieving documents are common operations that we do and by default MongoDB queires return the whole document as a respons
-when dealing with complex data structure there might be instances when fields are not useful for the query that we are trying to accomplish
-to help with readabillity fo the values returned, we can inlcude/exclude fileds from the response
-in this section we are trying to modify the response of MongoDB but not the document itself when we check the collection
*/

// INCLUSION

/* 
-allows us to include the indicated fields 
-the value provided is 1 to denote the field is being included
SYNTAX:
    db.collectionName.find({criteria}, {fieldName: 1})
    db.collectionName.find({criteria}, {fieldName: true})
*/
db.users.find(
    {firstName:"Jane"},
    {
        firstName: 1,
        lastName: 1,
        contact: 1
    }
);

// EXCLUSION

/* 
-allows us to include the excluded fields 
-the value provided is 0 to denote the field is being excluded
SYNTAX:
    db.collectionName.find({criteria}, {fieldName: 0})
    db.collectionName.find({criteria}, {fieldName: false})
*/
db.users.find(
    {firstName:"Jane"},
    {
        firstName: 0,
        lastName: 0,
        contact: 0
    }
);
/* 
look for a document but do not display the id field and include firstName, lastName, and contact
*/

// Supressing of ID field

/* 
-allows us to exlclude _id field in the returned document
-when using field projections, field inclusion and exclusion may be used at the same time
-this is ONLY APPLICABLE if we are suppressing/excluding ID
*/
db.users.find(
    {firstName:"Jane"},
    {
        firstName: 1,
        lastName:1,
        contact:1,
        _id: 0
    }
);

// 

db.users.find(
    {firstName:"Jane"},
    {
        firstName: 1,
        lastName:1,
        contact:{
            phone: true
        }
    }
);

// another way of coding it 
db.users.find(
    {firstName:"Jane"},
    {
        firstName:1,
        lastName:1,
        "contact.phone": 1
    }

);
// Suppressing embedded documents
db.users.find(
    {firstName:"Jane"},
    {
        "contact.phone": 0
    }
);

// Project Specific Array Elements in the Returned Array

// $slice operator - allows us to retrieve only 1 element that matches the search criteria

db.users.find(
    {
        "nameArr":{nameA:"Juan"}
    },
    {
        nameArr:{$slice : 1}
    }
);


// SECTION -Evaluation Query Operators
// $regex
/* 
-it allows us to find documents that match  a specific string pattern using regular expressions
SYNTAX:
    db.collectionName.find({field: {$regex: "pattern", $options: "$optionsValue"}});
*/

// Case-sensitive query -gets all with "N" strict casing
db.users.find({fristName: {$regex:"J"}});

// Case-insensitive query
db.users.find({fristName: {$regex:"j", $options: "$i"}});

// using or, this can also be done below

/* 
db.users.find({$or: [{firstName:{$regex:"n"}},{firstName:{$regex:"N"}}]});
*/

